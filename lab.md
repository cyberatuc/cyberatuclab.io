---
layout: page
title: Lab
permalink: /lab/
---
Cyber@UC manages a lab space in Mantei 516 & 513 (the engineering building). All are welcome to come to our weekly lab sessions every Monday from 6:00 to 8:00pm and work on building up the lab. We are currently working on setting up Promox on our servers, configuring networking for the lab, working on AWS CTF infrastructure, developing/maintaining various software and much more. Whether you're a sysadmin god or a complete newcomer, all are invited and encouraged to come and get some hands-on experience with enterprise-grade equipment!

## Main lab space (Mantei 516)
{% include image.html src="lab_entrance05172023.jpg" type="left" alt="The front door to our lab room, with students working inside" %}
Our main lab space is equipped with plenty of desks and computers for working and studying. Besides our weekly lab time, the lab is frequently open throughout the week, so feel free to stop by any time to do work or hang out. If you want access to the lab outside of normal times check with an executive to see if the room is unlocked.

## Server room (Mantei 513)
{% include image.html src="2018-08-27-lab-racks-chris.jpg" type="left" alt="Messy room with server racks, with Chris hiding behind one of them" %}
Thanks to a grant from the state of Ohio, we have received $200k worth of server equipment to use for malware research, red/blue team simulations, and more. <!--You can read more about how we're using this equipment in [this blog post]({{ site.url }}/blog/2018/08/31/security-operations-center-plans).-->
