#!/usr/bin/env bash
# Usage: ./android-studio.sh [android-studio-ide-linux.tar.gz]
 
# make sure we have root privs
sudo -v
 
# extract app to /opt
sudo tar -xvzf -C /opt $1
 
# create desktop entry config file
cat > /tmp/android-studio.desktop << EOF
[Desktop Entry]
Type=Application
Version=1.0
Name=Android Studio
Comment=Development toolset for Android apps
Path=/opt/android-studio/bin
Exec=/opt/android-studio/bin/studio.sh
Icon=/opt/android-studio/bin/studio.png
StartupWMClass=jetbrains-studio
Terminal=false
Categories=Development;IDE
EOF
 
# install desktop entry into system applications folder
sudo desktop-file-install --dir=/usr/share/applications /tmp/android-studio.desktop
 
# set up for Android emulator (fixes /dev/kvm permissions error)
sudo apt install qemu-kvm
sudo adduser $USER kvm
