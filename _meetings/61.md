---
title: "Running a Linux box securely"
date: 2018-07-25 18:30:00
meeting_number: 61
youtube: https://www.youtube.com/watch?v=AhXE0RKXi_4
---
This week, Hayden showed off some random stuff related to administering a Linux system, and talked about his home server setup.

## Running a Linux box securely
The slides for Hayden's presentation were written in Markdown and presented with a CLI tool called patat. The Markdown content of the slides has been copied below:

### What gear am I rocking?
* Home server running Debian, primarily used as a NAS
  * Parts list if you're into that: <https://pcpartpicker.com/list/bXvPKB>
* Also was sorta "the devops guy" at the startup I worked at
  * A bunch of Linux EC2 instances on AWS (mostly CentOS)

### Broad security goals
* Least privilege: isolate everything
  * Network, applications, files
* Patch, patch, patch

### iptables
CLI tool for configuring Linux firewall

* Chains: PREROUTING, INPUT, FORWARD, OUTPUT, POSTROUTING
* Tables: filter, nat, mangle, raw
* Most common config
  * Reject all new connections on INPUT (except localhost and specific ports)
  * Allow all on OUTPUT

### A typical rules.v4 file
```
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 443 -j ACCEPT
-A INPUT -j REJECT --reject-with icmp-host-prohibited
-A FORWARD -j REJECT --reject-with icmp-host-prohibited
COMMIT
```

### More advanced rules
#### OS fingerprinting
Reject TCP connections from Windows machines
```
-I INPUT -p tcp -m osf --genre Windows --log 1 --smart -j REJECT --reject-with icmp-host-prohibited
```
More info: <http://www.ioremap.net/2012/08/13/osf/>

. . .

#### String matching
Reject HTTP connections containing "banana69"
```
-I INPUT -p tcp -m tcp --dport 80 -m string --algo bm --string "banana69" -j REJECT --reject-with tcp-reset
```

. . .

#### So much more
<https://linux.die.net/man/8/iptables>

### Web servers
Basically everything has a web server tacked on it now

(gonna talk about Apache cuz I'm old school)

### HTTPS
* You need it.
* It's free so you have no excuse -- Let's Encrypt
  * Easiest way: [Certbot](https://certbot.eff.org/)

### Reverse proxies
* Because some 3rd party software just isn't trustworthy enough.
  * For example... <https://redd.it/91r295>

Very simple config in Apache:
```
ProxyPass "/foobar" "http://127.0.0.1:1234"
ProxyPassReverse "/foobar" "http://127.0.0.1:1234"
```

More info: <https://httpd.apache.org/docs/2.4/mod/mod_proxy.html>

### Access control
Restrict services by IP address, user account, etc

More info: <https://httpd.apache.org/docs/2.4/mod/mod_authz_core.html>

### SSH
* No password logins!
  * Need to have your public key in *~/.ssh/authorized\_keys*
* No root login!

_/etc/ssh/sshd\_config_:
```
ChallengeResponseAuthentication no
PasswordAuthentication no
UsePAM no
PermitRootLogin no
```

### SELinux
* Shifts the paradigm from "deny all" instead of "allow all"
* Every service can only access files with that type explicitly set

Example:

* Apache is only given access to say, */etc/httpd* and */var/www*
* Dennis accidentally sets */home/dennis/muh-bitcoins.txt* to chmod 644  
    (typical fucking Dennis)
* A web app on the server had a filesystem traversal vulnerability
* Attacker tries, but fails, to steal Dennis' bitcoins

<https://wiki.centos.org/HowTos/SELinux>

### Automatic package updates
For Debian/Ubuntu-like systems: `cron-apt`!

<https://debian-administration.org/article/162/A_short_introduction_to_cron-apt>

## Questions? :)
